?>"><?php 

	get_header(); 

	$user_info = get_userdata($post->post_author);
	$user_meta = get_user_meta($post->post_author);
	$post_categories = wp_get_post_categories( $post->ID );
	$cats = array();
	$cat_names = array();

	foreach($post_categories as $c){
		$cat = get_category( $c );
		$cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
		$cat_names[] = '<strong>'.$cat->name.'</strong>';
	};
?>
<section id="content">
  <div class="top-of-page">
    <nav>
      <ul>
        <li><a href="<?php ?>">Blog</a></li>
        <li><a href="<?php ?>">Guidelines</a></li>
        <li><a href="<?php ?>">About us</a></li>
        <li><a href="<?php ?>">Events</a></li>
      </ul>
    </nav>
  </div>
  <br class="clear" />
   <div class="articles">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <article class="blog">
      <div>
      <time>Veröffentlicht am <?php the_time('j. F, Y'); ?></time>
      <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php if(count($cats) >= 1) {?>
      <ul class="categories">
				<?php foreach($post_categories as $cat){ ?>
					<?php $c = get_category( $cat ); ?>
				<li><?php echo '<a href="'.get_category_link($c->term_id ).'" class="'.$c->slug.'">'.$c->cat_name.'</a>'; ?></li>
				<?php }; ?>
			</ul>
			<?php }; ?>
      <?php the_content(__('(weiterlesen...)')); ?>

			<?php comment_form(); ?>

      </div>
      <aside>
        <div class="author">
                 <?php echo get_avatar( $user_info->user_email ); ?>
                 <p class="name"><?php echo $user_info->display_name; ?><br />
                   <?php echo $user_meta[description][0]; ?></p>
                                                       <?php $twitter = end(explode('/', $user_info->user_url)); ?>
                 <p><a href="https://twitter.com/<?php echo $twitter ?>" class="twitter-follow-button" data-show-count="false">Follow @<?php echo $twitter ?></a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></p>
                 <p><a href="mailto:<?php echo $user_info->display_name; ?>">Email</a></p>
               </div>
				<?php if(count($cats) >= 1) { ?>
        <div class="related">
          <h3>Weitere Beitrage in der Kategorie 
						<?php echo implode($cat_names, ' und '); ?>
					</h3>
					<ul>
					<?php
					$cat_posts = new WP_Query(array('post_per_page' => 4, 'post__not_in' => array($post->ID), 'category__in' => $post_categories));
					while ($cat_posts->have_posts()) : $cat_posts->the_post();
					?>
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php  endwhile; ?>
          </ul>
        </div>
				<?php 
        }; ?>
      </aside>
    </article>
  <?php endwhile; else: ?>
  <p><?php _e('We could not find any posts.'); ?></p>
  <?php endif; ?>
  <br class="clear" />
  </div>
</section>

<footer>
  <p>coderswithoutborders proudly supports <a href="http://www.brug.cz/">brugcz</a>, <a href="http://www.rubyslava.sk">rubyslava</a> and <a href="http://viennarb.at/">vienna.rb</a></p>
  <br class="clear"/>
</footer>
