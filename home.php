<?php
/**
Template Name: Home Page
**/
get_header(); ?>

<section id="content">
  <div class="top-of-page">
    <nav>
      <ul>
        <li><a href="<?php ?>">Blog</a></li>
        <li><a href="<?php ?>">Guidelines</a></li>
        <li><a href="<?php ?>">About us</a></li>
        <li><a href="<?php ?>">Events</a></li>
      </ul>
    </nav>
  </div>
  <br class="clear" />
   <div class="articles">
     <?php
         $posts = new WP_Query(array(
              'tag' => 'featured',
							'posts_per_page' => 6 // Change accordingly
         ));
        $total_posts_query = new WP_Query(array('tag' => 'featured'));
        $total_posts = $total_posts_query->post_count;

        if( $posts->have_posts() ) {
                 $i = 0;
               while ($posts->have_posts()) : $posts->the_post(); 
                       $bback = get_post_meta($post->ID, 'box-color', true);
                       $post_categories = wp_get_post_categories( $post->ID ); 
                       $category = get_category($post_categories[0]);
                       
                       ?>
           <article class="<?php echo $category->slug; ?>">
             <header><a href="<?php echo get_category_link($category->term_id ) ?>"><?php echo $category->name; ?></a></header>
             <div>
               <h3><time><?php the_time('j. F, Y'); ?></time></h3>
               <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
               <?php the_excerpt(); ?>
            </div>
           </article>
         </div>
  <br class="clear" />
  </div>
  <?php
    if ($posts->post_count < $total_posts) { ?>
      <br class="clear" />
      <p style="text-align: center;">
        <a href="#" onclick="loadMorePosts();return false;" id="more_link">show more events</a>
      </p>
  <?php }; ?>
	<br class="clear" />
 </div>
</section>

<?php get_footer(); ?>