<?php
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Read more</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

add_filter('get_image_tag', 'add_image_frames', 10, 6);
function add_image_frames($html, $id, $alt, $title, $align, $size) {
	list($img_src, $width, $height) = image_downsize($id, $size);
  $cap = get_post_meta($id);
  
}
?>