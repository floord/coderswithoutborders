!DOCTYPE html>
<html>
<head>
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );


    ?></title>
 <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
    <?php wp_head(); ?>
   </head>
<?php

	$battr = array();

	$bback = get_post_meta($post->ID, 'body_background', true);

	if (!isset($bid)){
		$bid   = get_post_meta($post->ID, 'body_id', true);
	};

	if(is_front_page()){
		$bid = 'index-page';
	};

	if(is_home()){
		$bid = 'blog-page';
	};

	if(is_single()){
		$bid = 'post-page';
	}

	if(is_search() || is_category()){
		$bid = 'search-page';
	};

	if ($bback != ''){
		$battr['style'] = 'background:'.$bback.';';
	};
	
	if ($bid != ''){
		$battr['id'] = $bid;
	};
?>
<body <?php echo(urldecode(http_build_query($battr, ' '))); ?>>
<header>
  <div>
    <h1><a href="/"><span><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></span></a></h1>
    <nav>
      <?php wp_nav_menu( array( 'container' => false ) ); ?>
    </nav>
  	<form method="get" id="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
  		<input type="text" class="field" name="s" id="s" placeholder="<?php esc_attr_e( 'Search'); ?>" />
  	</form>
  </div>
  <br class="clear" />
</header>